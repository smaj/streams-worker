import logging
from uuid import uuid4

from aiohttp import web

import redis
import scheduler


async def init_app():
    worker_id = uuid4().hex
    app = web.Application()

    app["config"] = {
        "redis": {"host": "127.0.0.1", "group_name": "test", "streams": ["test"]},
        "worker_id": worker_id,
    }

    app.on_startup.append(redis.start_pool)
    app.on_startup.append(scheduler.start_scheduler)

    app.on_shutdown.append(scheduler.stop_scheduler)
    app.on_shutdown.append(redis.close_pool)

    return app


def main():
    logging.basicConfig(
        level=logging.DEBUG, format="[%(asctime)s] %(levelname)s %(message)s"
    )

    app = init_app()
    web.run_app(app)


if __name__ == "__main__":
    main()
