import asyncio
import logging
import random


async def dummy(app, stream, group_name, message):
    await asyncio.sleep((random.uniform(1, 3)))

    _, identifier, _ = message
    logging.info(f"processed {identifier}")

    await app["redis"].xack(stream, group_name, identifier)
    logging.debug(f"acknowledged {identifier}")
