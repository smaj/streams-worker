import asyncio
import logging

import aiojobs

import redis


async def start_scheduler(app):
    app["scheduler"] = await aiojobs.create_scheduler(
        limit=5, pending_limit=5, close_timeout=30
    )

    # Start background jobs
    app["scheduler_running"] = True
    asyncio.create_task(redis.read_new_messages(app))
    asyncio.create_task(redis.read_stale_messages(app))


async def stop_scheduler(app):
    app["scheduler_running"] = False

    for i in range(1, 60):
        logging.info(f"Tasks active: {app['scheduler'].active_count}")
        logging.info(f"Tasks pending: {app['scheduler'].pending_count}")
        if len(app["scheduler"]) > 0:
            await asyncio.sleep(1)
        else:
            logging.info("Clean shutdown")
            break

    await app["scheduler"].close()
