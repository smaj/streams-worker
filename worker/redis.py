import asyncio
import datetime
import logging

import aioredis
from aioredis.errors import ReplyError

import tasks


async def start_pool(app):
    app["redis"] = await aioredis.create_redis_pool(
        "redis://localhost", minsize=10, maxsize=10, encoding="utf-8"
    )


async def close_pool(app):
    app["redis"].close()
    await app["redis"].wait_closed()


async def read_new_messages(app):
    redis = app["redis"]

    # Create read group if not exists
    for stream in app["config"]["redis"]["streams"]:
        try:
            await redis.xinfo_groups(stream)

        except ReplyError:
            # https://github.com/aio-libs/aioredis/issues/503
            await redis.xadd(stream, {"test": "test"})
            await redis.xgroup_create(stream, app["config"]["redis"]["group_name"])

        if app["scheduler_running"]:
            asyncio.create_task(_read_new_messages(app, stream))


async def _read_new_messages(app, stream):
    # Acquire an exclusive connection for blocking operation
    redis = app["redis"]
    with await redis as r:
        messages = await r.xread_group(
            app["config"]["redis"]["group_name"],
            app["config"]["worker_id"],
            app["config"]["redis"]["streams"],
            1000,  # Timeout
            20,  # Max messages
            [">"],  # Read only unclaimed messages
        )

    for message in messages:
        await app["scheduler"].spawn(
            tasks.dummy(app, stream, app["config"]["redis"]["group_name"], message)
        )
    logging.debug("Read new messages task finished")

    if app["scheduler_running"]:
        asyncio.create_task(_read_new_messages(app, stream))


async def read_stale_messages(app, idle_time=10):
    redis = app["redis"]

    timestamp = (
        datetime.datetime.now() - datetime.timedelta(seconds=idle_time)
    ).strftime("%s%f")

    # Find all pending messages between any time in the past and timestamp
    for stream in app["config"]["redis"]["streams"]:
        messages = await redis.xpending(
            stream,
            app["config"]["redis"]["group_name"],
            "-",  # No lower time boundary
            timestamp,  # Upper time boundary
            10,  # Max messages
        )

        message_ids = [message_id for message_id, _, _, _ in messages]

        if message_ids:
            claimed_msgs = await redis.xclaim(
                stream,
                app["config"]["redis"]["group_name"],
                app["config"]["worker_id"],
                idle_time * 1000,  # Claim messages with idle timer than 10 seconds
                *message_ids
            )

            for message in claimed_msgs:
                await app["scheduler"].spawn(
                    tasks.dummy(
                        app,
                        stream,
                        app["config"]["redis"]["group_name"],
                        (stream, *message),  # Make message compatible with xread_group
                    )
                )

    logging.debug("Read stale messages task finished")

    if app["scheduler_running"]:
        await asyncio.sleep(3)
        asyncio.create_task(read_stale_messages(app))
