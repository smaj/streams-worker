import asyncio
import aioredis

loop = asyncio.get_event_loop()


async def go():
    pool = await aioredis.create_redis_pool("redis://localhost", minsize=5, maxsize=10)
    for i in range(0, 1000):
        await pool.xadd("test", {"test": 1})
    # graceful shutdown
    pool.close()
    await pool.wait_closed()


loop.run_until_complete(go())
